/*
*************************************************************************************************************************************************************************************************************
FIST IOT WORKSHOP 2017
*************************************************************************************************************************************************************************************************************

Name                  : Soil Moisture Sensor Server Send
Hardware Platform     : ESP 12E NodeMcu V3
Toolchain             : Arduino IDE V1.8.3, ESP8266 core for Arduino V2.3.0.
Version               : 1.0
Date created          : 08/08/2017
Author                : Mugilan M 
Contact               : mugilan_july@yahoo.co.in
Description           : Reads soil moisture data from sensor and send to server via TCP socket every 5 Seconds.

*************************************************************************************************************************************************************************************************************

Copyright 2017 Mugilan M

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************************************************************************************************************************************
*/

// Pin Number: Sensor Module A0 -> NodeMCU A0

#include <ESP8266WiFi.h>

WiFiClient client;

const char* ssid = "SSID";
const char* password = "PASSWD";

IPAddress server_ip(192,168,1,101);  

int server_port = 12345;

int sensor_value = 0;

void setup() {
   Serial.begin(115200);

   WiFi.begin(ssid, password);

   Serial.println("");
   Serial.println("");
 
  while (WiFi.status() != WL_CONNECTED) {
     delay(500);
     Serial.println("Trying to Connect WiFi Network");
  }

    
}


void loop() {

    Serial.println("Starting connection...");

    sensor_value = analogRead(A0);

    if (client.connect(server_ip, server_port)) {
      Serial.println("Connected to Server");
      client.println("SoilMoisture
1 Data:");
      client.println(sensor_value);

      Serial.println("Closing Connection");
      client.stop();
    }

    else
    {
      Serial.println("Connection Failed");
    }	

    delay(5000);
}

