/*
*************************************************************************************************************************************************************************************************************
FIST IOT WORKSHOP 2017
*************************************************************************************************************************************************************************************************************

Name                  : UART LED Control
Hardware Platform     : ESP 12E NodeMcu V3
Toolchain             : Arduino IDE V1.8.3, ESP8266 core for Arduino V2.3.0.
Version               : 1.0
Date created          : 08/08/2017
Author                : Mugilan M 
Contact               : mugilan_july@yahoo.co.in
Description           : Controls on-board or external LED through serial command.

*************************************************************************************************************************************************************************************************************

Copyright 2017 Mugilan M

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************************************************************************************************************************************
*/

// Serial Command: 'y' -> turns LED ON; 'n' -> turns LED OFF

#define LED LED_BUILTIN
//#define LED 5

char ser_recv;

void setup() {
  Serial.begin(115200);
  pinMode(LED, OUTPUT);   // Active Low Logic : 0 -> LED ON; 1 -> LED OFF.
  digitalWrite(LED, HIGH);    
}

void loop() {

  if(Serial.available() > 0)
  {
  ser_recv = Serial.read();
  
  if(ser_recv == 'y')
  {
  digitalWrite(LED, LOW);   
  }

  else
  {
    if(ser_recv == 'n')
    {
      digitalWrite(LED, HIGH);  
    }
  }

  }
  

}
