#!/usr/bin/env python

"""PY_Simple_Server_V_1_0.py: A simple TCP server implementation. Creates a TCP socket and accepts a tcp connection from a node, prints data received from node and sends a message back to node.Finally closes socket."""

__author__ = "Mugilan M"
__copyright__ = "Copyright 2017 Mugilan M"
__license__ = "GPLV3"
__version__ = "1.0"


import socket              

s = socket.socket()         
#host = socket.gethostname() 
host = "192.168.43.227"	# Change with your local ip address
#print host
port = 12345               
s.bind((host, port))        

s.listen(40)                 


while True:
   c, addr = s.accept()     
   #print 'Got connection from', addr
   data =  c.recv(1024)
   print data
   c.send("Message Received")
   c.close()
